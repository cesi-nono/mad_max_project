package Model;

import java.sql.*;

public class CAD {
    private String connectionUrl, login, psw;
    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    public CAD() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        this.connectionUrl = "jdbc:mysql://localhost/mad_max_project?serverTimezone=UTC";
        this.login = "root";
        this.psw = "";

        this.con = DriverManager.getConnection(this.connectionUrl, this.login, this.psw);
    }

    public ResultSet getRows(String rq_sql, String resultSetName) throws SQLException {
        // TODO: TROUVER A QUOI SERT CE RESULT SET NAME
        this.stmt = this.con.createStatement();
        this.rs = this.stmt.executeQuery(rq_sql);

        return this.rs;
    }

    public void actionRows(String rq_sql) throws SQLException {
        this.stmt = this.con.createStatement();
        this.stmt.executeUpdate(rq_sql);
    }
}

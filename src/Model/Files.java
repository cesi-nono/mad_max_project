package Model;

import java.io.*;

public class Files {
    public static String getData(String path) throws Exception {
        InputStream stream = new FileInputStream(path);
        StringBuilder answer = new StringBuilder();

        for(int a = stream.read(); a != -1; a = stream.read()){
            answer.append((char)a);
        }

        stream.close();

        return answer.toString();
    }

    public static void setData(String path, String data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(data == null ? "" : data);
        writer.close();
    }
}

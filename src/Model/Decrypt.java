package Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

public class Decrypt {
    public static String decrypt(String data, String key, HashSet<String> dictionary){
        if (key.length() == 12) {
            return decryptFullKey(data, key);
        } else {
            return bruteforce(data, key, dictionary);
        }
    }

    private static String bruteforce(String data, String key, HashSet<String> dictionary) {
        for (char i = 'a'; i <= 'z'; i++) {
            if (key.length() + 1 == 12) {
                System.out.println(key + i);

                String decryptedData = decryptFullKey(data, key + i);

                // Split the data by words, then select a random word and check if it matches
                String[] splittedMessage = decryptedData.split("[\\s,.?!'\"]+");

                boolean isCorrect = true;
                for (int j = 0; j < splittedMessage.length; j++) {
                    if (!dictionary.contains(splittedMessage[j].toLowerCase())) {
                        isCorrect = false;
                    }
                }

                if (isCorrect) {
                    System.out.println("THE KEY IS " + key + i);
                    return decryptedData;
                }
            } else {
                String subBruteforce = bruteforce(data, key + i, dictionary);
                if (subBruteforce != null) {
                    return subBruteforce;
                }
            }
        }
        return null;
    }

    private static String decryptFullKey(String data, String key) {
        StringBuilder sb1;
        char c1, c2, c3;
        int i, ii;

        ii = 0;

        sb1 = new StringBuilder();

        for (i = 0; i < data.length(); i++) {
            c1 = data.charAt(i);
            c2 = key.charAt(ii);
            c3 = (char) (c1 ^ c2);

            sb1.append(c3);

            ii++;

            if (ii == key.length()) {
                ii = 0;
            }
        }
        return sb1.toString();
    }
}

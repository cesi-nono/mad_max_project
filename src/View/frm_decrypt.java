package View;

import Controller.wkf_decrypt;

import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import javax.imageio.ImageIO;

public class frm_decrypt {
    public frm_decrypt() {
        JFrame window = new JFrame("File decryption panel");
        JPanel mainPanel = new JPanel() {
            @Override public void paintComponent(Graphics g) { // Background Image
                super.paintComponent(g);

                try {
                    g.drawImage(ImageIO.read(new File("assets/img/form_bg.png")), 0, 0, this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        mainPanel.setLayout(new GridBagLayout());

        JPanel row1 = new JPanel();     row1.setOpaque(false);
        JPanel row2_1 = new JPanel();   row2_1.setOpaque(false);
        JPanel row2_2 = new JPanel();   row2_2.setOpaque(false);
        JPanel row3 = new JPanel();     row3.setOpaque(false);
        JPanel row4_1 = new JPanel();   row4_1.setOpaque(false);
        JPanel row4_2 = new JPanel();   row4_2.setOpaque(false);
        JPanel row5 = new JPanel();     row5.setOpaque(false);
        JPanel row6 = new JPanel();     row6.setOpaque(false);
        JPanel row7 = new JPanel();     row7.setOpaque(false);
        JPanel row8 = new JPanel();     row8.setOpaque(false);

        // Source path
        JButton b1 = new JButton("Browse");
        JLabel lbl1 = new JLabel("Encrypted file source path");
        JTextField text1 = new JTextField("", 25);

        lbl1.setForeground(Color.white);
        b1.addActionListener(e -> {
            try {
                String path = chooseFile();
                if (path != null) {
                    text1.setText(path);
                    text1.setToolTipText(text1.getText());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        row1.add(lbl1);
        row2_1.add(text1);
        row2_2.add(b1);

        // Destination path
        JButton b2 = new JButton("Browse");
        JLabel lbl2 = new JLabel("Destination path for decrypted data");
        JTextField text2 = new JTextField("", 25);

        lbl2.setForeground(Color.white);
        b2.addActionListener(e -> {
            try {
                String path = chooseFile();
                if (path != null) {
                    text2.setText(path);
                    text2.setToolTipText(text2.getText());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        row3.add(lbl2);
        row4_1.add(text2);
        row4_2.add(b2);

        // Key
        JTextField text3 = new JTextField("", 15);
        JLabel lbl3 = new JLabel("Key");

        lbl3.setForeground(Color.white);
        text3.setHorizontalAlignment(SwingConstants.CENTER);

        row5.add(lbl3);
        row6.add(text3);

        // Decrypt button
        JButton b3 = new JButton("Decrypt");

        b3.addActionListener(e -> {
            if (text1.getText().isEmpty() || text2.getText().isEmpty()) {
                JOptionPane.showMessageDialog(window, "Don't let paths empty!", "Decryption", JOptionPane.WARNING_MESSAGE);
                return;
            }

            if (text3.getText().length() > 12) {
                JOptionPane.showMessageDialog(window, "Key must not exceed 12 characters!", "Decryption", JOptionPane.WARNING_MESSAGE);
                return;
            }

            try {
                if (wkf_decrypt.pcs_decrypt(text1.getText(), text2.getText(), text3.getText())) {
                    JOptionPane.showMessageDialog(window, "SUCCESS", "Decryption", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(window, "WRONG KEY", "Decryption", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        row7.add(b3);

        // Add authentication account link
        JLabel lblRegister = new JLabel("Add authentication account");

        lblRegister.setForeground(Color.BLUE.darker());
        lblRegister.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lblRegister.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new frm_register();
            }
        });

        row8.add(lblRegister);



        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row1, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        mainPanel.add(row2_1, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        mainPanel.add(row2_2, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row3, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        mainPanel.add(row4_1, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        mainPanel.add(row4_2, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row5, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row6, gbc);

        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row7, gbc);

        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(row8, gbc);

        window.setContentPane(mainPanel);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(450, 400);
        window.setResizable(false);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }

    public String chooseFile() throws IOException{
        JFileChooser dialog = new JFileChooser(new File("."));
        File file;

        if(dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            file = dialog.getSelectedFile();

            return file.getAbsolutePath();
        }
        return null;
    }
}

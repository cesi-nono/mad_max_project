package View;

import Controller.wkf_cpte;
import Model.CAD;
import Model.Map_P;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.Arrays;

public class frm_register {
    private JFrame window;
    private JTextField usernameField;
    private JPasswordField passwordField, passwordField2;

    /**
     *  Init the auth window.
     */
    public frm_register() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        // Window properties
        this.window = new JFrame("Sign up");
        this.window.setBounds(100, 100, 542, 383);
        this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.window.getContentPane().setLayout(null);

        // Username field
        this.usernameField = new JTextField();
        this.usernameField.setBounds(226, 86, 147, 34);
        this.window.getContentPane().add(this.usernameField);
        this.usernameField.setColumns(10);

        // Password field
        this.passwordField = new JPasswordField();
        this.passwordField.setBounds(226, 131, 147, 33);
        this.window.getContentPane().add(this.passwordField);
        this.passwordField2 = new JPasswordField();
        this.passwordField2.setBounds(226, 178, 147, 33);
        this.window.getContentPane().add(this.passwordField2);

        // Button Sign Up
        JButton btnLogIn = new JButton("Sign up");
        btnLogIn.setBounds(226, 222, 147, 23);
        btnLogIn.addActionListener(e -> {
            if (Arrays.equals(this.passwordField.getPassword(), this.passwordField2.getPassword()) && !this.usernameField.getText().isEmpty()) {
                try {
                    wkf_cpte.addUser(this.usernameField.getText(), new String(this.passwordField.getPassword()));
                    this.window.setVisible(false);
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(this.window, "Check you entered a username and matching passwords!", "Registration", JOptionPane.WARNING_MESSAGE);
            }
        });
        this.window.getRootPane().setDefaultButton(btnLogIn);
        this.window.getContentPane().add(btnLogIn);

        // Label username
        JLabel lblUser = new JLabel("New username:");
        lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblUser.setBounds(128, 90, 104, 26);
        this.window.getContentPane().add(lblUser);

        // Label password
        JLabel lblPassword = new JLabel("New password:");
        lblPassword.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblPassword.setBounds(128, 131, 104, 26);
        this.window.getContentPane().add(lblPassword);
        JLabel lblPassword2 = new JLabel("Confirm new password:");
        lblPassword2.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblPassword2.setBounds(75, 176, 177, 26);
        this.window.getContentPane().add(lblPassword2);

        // Background image
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("assets/img/form_bg.png"));
        lblNewLabel.setBounds(0, 0, 526, 344);
        this.window.getContentPane().add(lblNewLabel);

        // Set window visible
        this.window.setLocationRelativeTo(null);
        this.window.setVisible(true);
    }
}

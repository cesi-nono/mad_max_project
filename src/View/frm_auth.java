package View;

import Controller.wkf_cpte;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.Arrays;

public class frm_auth {

    private JFrame window;
    private JTextField usernameField;
    private JPasswordField passwordField;

    /**
     *  Init the auth window.
     */
    public frm_auth() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        // Window properties
        this.window = new JFrame("Sign in");
        this.window.setBounds(100, 100, 542, 383);
        this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.window.getContentPane().setLayout(null);

        // Password field
        this.passwordField = new JPasswordField();
        this.passwordField.setBounds(226, 131, 147, 33);
        this.window.getContentPane().add(this.passwordField);

        // Button log in
        JButton btnLogIn = new JButton("Sign in");
        btnLogIn.setBounds(226, 175, 147, 23);
        btnLogIn.addActionListener(e -> {
            try {
                if (wkf_cpte.pcs_authentifier(usernameField.getText(), new String(passwordField.getPassword()))) {
                    this.window.setVisible(false);
                    new frm_decrypt();
                } else {
                    JOptionPane.showMessageDialog(window, "Wrong login or password!", "Authentication", JOptionPane.ERROR_MESSAGE);
                }
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        this.window.getRootPane().setDefaultButton(btnLogIn);
        this.window.getContentPane().add(btnLogIn);

        // Label username
        JLabel lblUser = new JLabel("Username: ");
        lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblUser.setBounds(128, 90, 104, 26);
        this.window.getContentPane().add(lblUser);

        // Label password
        JLabel lblPassword = new JLabel("Password: ");
        lblPassword.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblPassword.setBounds(128, 131, 104, 26);
        this.window.getContentPane().add(lblPassword);

        // Username field
        this.usernameField = new JTextField();
        this.usernameField.setBounds(226, 86, 147, 34);
        this.window.getContentPane().add(this.usernameField);
        this.usernameField.setColumns(10);


        // Background image
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("assets/img/form_bg.png"));
        lblNewLabel.setBounds(0, 0, 526, 344);
        this.window.getContentPane().add(lblNewLabel);

        // Set window visible
        this.window.setLocationRelativeTo(null);
        this.window.setVisible(true);
    }
}
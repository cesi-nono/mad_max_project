package Controller;

import Model.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class wkf_cpte {
    public static boolean pcs_authentifier(String login, String password) throws SQLException, ClassNotFoundException {
        CAD cad = new CAD();
        ResultSet rs = cad.getRows(Map_P.selectIDbyLoginPassword(login, password), "AuthMatchRS");

        if (rs.next()) {
            // Authentication successful
            return true;
        } else {
            // Authentication failure
            return false;
        }
    }

    public static void addUser(String login, String password) throws SQLException, ClassNotFoundException {
        CAD cad = new CAD();
        cad.actionRows(Map_P.insertUser(login, password));
    }
}

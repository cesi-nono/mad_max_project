package Controller;

import Model.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

public class wkf_decrypt {
    public static boolean pcs_decrypt(String source_path, String destination_path, String key) throws Exception {
        // Get crypted data from file
        String cryptedData = Files.getData(source_path);

        CAD cad = new CAD();

        if (key.length() == 12) {
            // Decrypt data
            String decryptedData = Decrypt.decrypt(cryptedData, key, null); // Key is given by user through view

            // Split the data by words, then select a random word and check if it matches
            String[] splittedMessage = decryptedData.split("[\\s,.?!'\"]+");
            int wordIndex = ThreadLocalRandom.current().nextInt(splittedMessage.length);

            ResultSet rs = cad.getRows(Map_Dic.selectWord(splittedMessage[wordIndex].toLowerCase()), "DictionaryMatchRS");

            if (rs.next()) {
                // Dictionary match, key is CORRECT
                Files.setData(destination_path, decryptedData);
                return true;
            } else {
                // No match, key is NOT CORRECT
                return false;
            }
        } else {
            HashSet<String> dictionary = new HashSet<>();
            ResultSet rs = cad.getRows(Map_Dic.selectAllWords(), "DictionaryRS");

            while (rs.next()) {
                dictionary.add(rs.getString("mot"));
            }

            String decryptedData = Decrypt.decrypt(cryptedData, key, dictionary);
            Files.setData(destination_path, decryptedData);
            return true;
        }
    }
}
